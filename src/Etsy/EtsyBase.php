<?php

namespace Etsy;

require( __DIR__ . '/../../vendor/autoload.php');

use \Httpful\Request;

class EtsyBase {

	private $api_base = 'https://openapi.etsy.com/';
	private $api_version = 'v2';
	private $api_key = '';

	function __construct($options) {

		if( isset($options['api_key']) ) {
			$this->api_key = $options['api_key'];
		} else {
			echo "Etsy API Key is Required" . PHP_EOL;
			die;
		}

	}

	function get($resource_uri, $params) {
		$response = Request::get($this->api_base . $this->api_version . $resource_uri . '?api_key=' . $this->api_key . '&' . $params)
			->send();

		return $response;
	}

}