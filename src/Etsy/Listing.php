<?php

namespace Etsy;

class Listing extends EtsyBase {

	function findAllShopListingsActive($params) {
		if(!isset($params['shop_id'])) {
			die();
		}

		$resource_uri = '/shops/' . $params['shop_id'] . '/listings/active';

		//Not needed in the Query String
		unset($params['shop_id']);

		$params = http_build_query($params);

		$response = $this->get($resource_uri, $params);

		return $response;

	}

}